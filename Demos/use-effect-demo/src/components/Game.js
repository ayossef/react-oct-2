import React, { useEffect, useState } from 'react'

function Game() {
    const [message, setMessage] = useState("")
    const [index, setIndex] = useState(0)
    const list = ["Hi", "Hello", "Welcome", "Good day"]
// Called only when the mentioned status gets updated
    useEffect(() => {
        console.log("Index or message has changed")
      }, [index,message])
// Called everytime
      useEffect(() => {
        console.log("Component has been mounted")
      }, )
// Called only one time
      useEffect(() => {
        console.log("No status tracked")
      }, [])
      
      

    const getNextMessage = () =>{
        setIndex(index + 1)
        if(index === list.length){
            setIndex(0)
        }
        setMessage(list[index])
    }
  return (
    <div>
        <h1>Game</h1>
        <button onClick={getNextMessage}>Display Message</button>
        <p>{message}</p>
    </div>
  )
}

export default Game