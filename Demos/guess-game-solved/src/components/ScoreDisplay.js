import React from 'react'
import { scoreContext } from './GuessGame'
import { useContext } from 'react'
function ScoreDisplay() {
const scoreStore = useContext(scoreContext)
  return (
    <div>
        <h3>
            ScoreDisplay
        </h3>
        <p>{scoreStore.storedMessage}</p>
    </div>
  )
}

export default ScoreDisplay