import React, { createContext } from 'react'
import { useState } from 'react'
import ScoreDisplay from './ScoreDisplay';

export const scoreContext = createContext();

function GuessGame(props) {
    const [message, setMessage] = useState("")
    const [userGuessValue, setUserGuessValue] = useState(0)
    const [score, setScore] = useState(0)
    const [playGameEnabled, setPlayGameEnabled] = useState(true)
    var remainingTrails = 5
    const scoreStore = {
        storedMessage: message,
        storedScore: score
    }
    const failed = () => {
        remainingTrails -= 1
        if(remainingTrails === 0){
            setPlayGameEnabled(false)
        }
    }
    const success = () => {
        setScore(10*remainingTrails)
    }
    // disabled after 5 wrong trials and enabled again when new game is played
    // disabled after winning a game
    const checkValue = ()=>{
        // Hints
        if(userGuessValue > props.num) {
            setMessage("Guess Lower")
            failed()
        }else if(userGuessValue < props.num) {
            setMessage("Guess Higher")
            failed()
        }
        else if(Number(userGuessValue) === props.num){
            setMessage("Congraulations")
            success()
        }else {
            setMessage("Try harder ...")
            failed()
        }
    }
  return (
   <scoreContext.Provider value={scoreStore}>
     <div>
        <h3>
        GuessGame
        </h3>
      <input placeholder='Enter your Guess Value' 
          onChange={(event)=>{setUserGuessValue(event.target.value)}}>
      </input>
    <br></br>
    <button onClick={checkValue} disabled={!playGameEnabled}>Feeling lucky ... </button>

    <ScoreDisplay></ScoreDisplay>
    </div>
   </scoreContext.Provider>
  )
}

export default GuessGame