import React from 'react'
import { useState, useEffect } from 'react'
import { useFetch } from '../hooks/useFetch'
function Users() {
    const url= "http://localhost:3000/users"
   const usersList = useFetch(url)
   
  return (
    <div>
        <h3>Users</h3>
       {usersList
       .map(
        (user)=>
       <p>{user.id} - {user.username} is reachable @ {user.email} <button id={user.id}>show products</button></p>
       )}
    </div>
  )
}

export default Users