import React from 'react'
import Users from './Users'
import Products from './Products'

function Home() {
  return (
    <div>
        <h1>Home</h1>
        <Users></Users>
        <Products></Products>
    </div>
  )
}

export default Home