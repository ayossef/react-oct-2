import {render, screen} from '@testing-library/react'
import Home from './Home'
import { useFetch } from '../hooks/useFetch'
import Users from './Users'

test('should contain users component', () => { 
    // AAA

    // Arrange
    render(<Home />)

    // Act
    const usersHeader = screen.getByText('Users')

    // Assert
    expect(usersHeader).toBeInTheDocument()
 })

 test('should contain three users', () => { 
    // Arrange
    const url = "http://localhost:3000/users"
    // Act>
   render(<Users />)

    // Assert
    expect(usersList.length).toBe(3)
  })

  test('should sum correctly', () => { 
    expect(2).toBe(1+1)
   })