import React from 'react'
import { useFetch } from '../hooks/useFetch'
import { useState } from 'react'

function Products() {
  const url = "http://localhost:3000/products"
  const productsList = useFetch(url)
  console.log(productsList)
  return (
    <div>
        <h3>Products</h3>
        {productsList.map((product)=>
        <p>{product.id} - {product.product_name} : {product.price} </p>
        )}
    </div>
  )
}

export default Products