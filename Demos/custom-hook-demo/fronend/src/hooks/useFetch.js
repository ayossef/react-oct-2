import { useEffect, useState } from "react"

export const useFetch = (url) => {
    const [data, setData] = useState([])
    useEffect(() => {
      fetch(url)
      .then((resp)=> resp.json())
      .then((jsonresp)=> setData(jsonresp))
    }, [url])
    return data
}