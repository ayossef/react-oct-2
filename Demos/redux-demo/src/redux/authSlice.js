import { createSlice } from "@reduxjs/toolkit";

export const authSlice = createSlice({
    name:'auth',
    initialState:{
        authState: false,
        username: ""
    }, 
    reducers:{
        login:(state)=>{state.authState=true},
        logout:(state)=>{state.authState=false; state.username=""},
        setUsername:(state,action)=>{state.username = action.payload.username} 
    }
})

export const {login, logout, setUsername} = authSlice.actions
export default authSlice.reducer