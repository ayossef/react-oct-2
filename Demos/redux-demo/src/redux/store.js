import { combineReducers, configureStore } from "@reduxjs/toolkit";

//import reducer from './authSlice'
import authReducer from './authSlice'
let reducer = combineReducers({
    auth: authReducer
})

export default configureStore({
    reducer
})