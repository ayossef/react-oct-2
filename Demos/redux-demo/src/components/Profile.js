import React from 'react'
import { useDispatch } from 'react-redux'
import { logout } from '../redux/authSlice'
function Profile() {
  let dispatchQ = useDispatch()
  const logoutAction = () => {
    dispatchQ(logout())
  }
  return (
    <div>
      <h1>Profile</h1>
      <button onClick={logoutAction}>Logout</button>
    </div>
  )
}

export default Profile