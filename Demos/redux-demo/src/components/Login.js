import React from 'react'
import { useDispatch } from 'react-redux'
import { login } from '../redux/authSlice'

function Login() {
  const dispatchQ = useDispatch()
  const loginAction = () => {
    dispatchQ(login())
  }
  // 1. input to capture the username and place it inside the store
  return (
    <div>
      <h1>Login</h1>
      <button onClick={loginAction}>Login</button>
    </div>
  )
}

export default Login