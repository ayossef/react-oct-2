import React from 'react'
import Login from './Login'
import Dashboard from './Dashboard'
import Profile from './Profile'
import { useSelector } from 'react-redux'

function Home() {
  const authFlag = useSelector((state)=> state.auth.authState)
  return (
    <div>Home
        {!authFlag && <Login></Login>}
        {authFlag && <Dashboard></Dashboard>}
        {authFlag && <Profile></Profile>}
    </div>
  )
}

export default Home