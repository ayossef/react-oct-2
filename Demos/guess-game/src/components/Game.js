import React from 'react'
import { useState } from 'react'
import GuessGame from './GuessGame'

function Game() {
    const [randomNumber, setRandomNumber] = useState(0)
    const generateRandomNumber = () => {
        setRandomNumber(Math.round(Math.random()*10))
        
    }

  return (
    <div>
        <h1>Game</h1>
        <button 
            onClick={generateRandomNumber}>New Game
        </button>
    
        <GuessGame num={randomNumber}></GuessGame>
    </div>

  )
}

export default Game