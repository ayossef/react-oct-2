import React, { createContext } from 'react'
import { useState } from 'react'
import ScoreDisplay from './ScoreDisplay';

export const scoreContext = createContext();

function GuessGame(props) {
    const [message, setMessage] = useState("")
    const [userGuessValue, setUserGuessValue] = useState(0)
    const [score, setScore] = useState(0)
    var remainingTrails = 5
    const scoreStore = {
        storedMessage: message,
        storedScore: score
    }
    // disabled after 5 wrong trials and enabled again when new game is played
    // disabled after winning a game
    const checkValue = ()=>{
        // Hints
        if(Number(userGuessValue) === props.num){
            setMessage("Congraulations")
            setScore(10*remainingTrails)
        }else {
            setMessage("Try harder ...")
            remainingTrails -= 1
        }
    }
  return (
   <scoreContext.Provider value={scoreStore}>
     <div>
        <h3>
        GuessGame
        </h3>
      <input placeholder='Enter your Guess Value' 
          onChange={(event)=>{setUserGuessValue(event.target.value)}}>
      </input>
    <br></br>
    <button onClick={checkValue}>Feeling lucky ... </button>

    <ScoreDisplay></ScoreDisplay>
    </div>
   </scoreContext.Provider>
  )
}

export default GuessGame